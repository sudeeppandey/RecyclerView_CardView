package pandey.sudeep.recyclerview;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    //private RecyclerView.LayoutManager mLayoutManager;
    private LinearLayoutManager linearLayoutManager;
    private MyAdapter mAdapter;
    private List<MyCarrier> data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        data = new ArrayList<MyCarrier>();

        data.add(0,new MyCarrier("Barcelona",R.drawable.barcelona));
        data.add(1,new MyCarrier("Cordoba",R.drawable.cordoba));
        data.add(2,new MyCarrier("Gran Canaria",R.drawable.gran_canaria));
        data.add(3,new MyCarrier("Granada",R.drawable.granada));
        data.add(4,new MyCarrier("Ibiza",R.drawable.ibiza));
        data.add(5,new MyCarrier("Lanzarote",R.drawable.lanzarote));
        data.add(6,new MyCarrier("Madrid",R.drawable.madrid));
        data.add(7,new MyCarrier("Balearic",R.drawable.balearic));
        data.add(8,new MyCarrier("Majorca",R.drawable.majorca));
        data.add(9,new MyCarrier("Malaga",R.drawable.malaga));
        data.add(10,new MyCarrier("Palma",R.drawable.palma));
        data.add(11,new MyCarrier("Sevilla",R.drawable.sevilla));
        data.add(12,new MyCarrier("Teida",R.drawable.teide));
        data.add(13,new MyCarrier("Tenerife",R.drawable.tenerife));
        data.add(14,new MyCarrier("Valencia",R.drawable.valencia));

        recyclerView = findViewById(R.id.recyclerView);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        recyclerView.setHasFixedSize(true);

        linearLayoutManager = new LinearLayoutManager(this);

        //mLayoutManager = new GridLayoutManager(this,2);
        //mLayoutManager = new StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);

        mAdapter = new MyAdapter(data);
        recyclerView.setAdapter(mAdapter);

        /*
        mAdapter.setListener(new MyAdapter.Listener(){

            @Override
            public void onClick(int position) {
                //findViewById(R.id.main_button).setVisibility(View.INVISIBLE);
            }
        });
        */

        final EndlessRecyclerViewScrollListener scrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {

             List<MyCarrier> morePlaces;
             int curSize;

            @Override
            public void onLoadMore(int page, int totalItemsCount, final RecyclerView view) {

                morePlaces=new ArrayList<>();
                morePlaces.add(0,new MyCarrier("Costa Del Sol",R.drawable.costadelsol));
                morePlaces.add(1,new MyCarrier("Donostia",R.drawable.donostia));
                morePlaces.add(2,new MyCarrier("Fuerteventura",R.drawable.fuerteventura));
                morePlaces.add(3,new MyCarrier("Santiago",R.drawable.santiago));
                morePlaces.add(4,new MyCarrier("Toledo",R.drawable.toledo));

                curSize = mAdapter.getItemCount();
                data.addAll(morePlaces);

                view.post(new Runnable() {
                    @Override
                    public void run() {
                        mAdapter.notifyItemRangeInserted(curSize, morePlaces.size() - 1);
                    }
                });
            }};

        recyclerView.addOnScrollListener(scrollListener);

    }
}